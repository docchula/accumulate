import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionData,
  collectionSnapshots,
  deleteDoc,
  doc,
  docData,
  Firestore,
  orderBy,
  query,
  serverTimestamp,
  updateDoc,
  where
} from '@angular/fire/firestore';
import { Functions, httpsCallable } from '@angular/fire/functions';
import { deleteObject, ref, Storage, uploadBytes } from '@angular/fire/storage';
import { getDownloadURL } from 'firebase/storage';
import { combineLatest } from 'rxjs';
import { filter, first, map, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import {
  chapterConverter,
  courseConverter,
  questionConverter,
  questionImageConverter,
  questionResponseConverter,
  userResponseConverter
} from '../firebase-converter';
import { Chapter } from '../shared/chapter';
import { Course } from '../shared/course';
import { Question } from '../shared/question';
import { QuestionImage } from '../shared/question-image';
import { AuthService } from './auth.service';

@Injectable()
export class QuestionService {
  constructor(
    private firestore: Firestore,
    private storage: Storage,
    private auth: AuthService,
    private functions: Functions
  ) {}

  listCourses() {
    // return this.affs
    //   .collection<Course>('courses', ref => {
    //     return ref.where('deleted', '==', false).orderBy('code');
    //   })
    //   .snapshotChanges();
    return collectionSnapshots(
      query(
        collection(this.firestore, 'courses'),
        where('deleted', '==', false),
        orderBy('code')
      ).withConverter(courseConverter)
    );
  }

  getCourse(courseKey: string) {
    // return this.affs
    //   .collection<Course>('courses')
    //   .doc<Course>(courseKey)
    //   .valueChanges();
    return docData(
      doc(collection(this.firestore, 'courses'), courseKey).withConverter(
        courseConverter
      )
    );
  }

  createCourse(course: Course) {
    // return this.affs
    //   .collection<Course>('courses')
    //   .add({ ...course, deleted: false });
    return addDoc(
      collection(this.firestore, 'courses').withConverter(courseConverter),
      {
        ...course,
        deleted: false
      }
    );
  }

  updateCourse(courseKey: string, data: Partial<Course>) {
    // return this.affs
    //   .collection('courses')
    //   .doc<Course>(courseKey)
    //   .update(data);
    return updateDoc(
      doc(collection(this.firestore, 'courses'), courseKey).withConverter(
        courseConverter
      ),
      data
    );
  }

  removeCourse(courseKey: string) {
    // return this.affs
    //   .collection('courses')
    //   .doc<Course>(courseKey)
    //   .update({ deleted: true });
    return updateDoc(
      doc(collection(this.firestore, 'courses'), courseKey).withConverter(
        courseConverter
      ),
      {
        deleted: true
      }
    );
  }

  listChapter(courseKey: string) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection<Chapter>('chapters', ref => {
    //     return ref.where('deleted', '==', false);
    //   })
    //   .snapshotChanges();
    return collectionSnapshots(
      query(
        collection(
          doc(collection(this.firestore, 'courses'), courseKey),
          'chapters'
        ),
        where('deleted', '==', false)
      ).withConverter(chapterConverter)
    );
  }

  getChapter(courseKey: string, chapterKey: string) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc<Chapter>(chapterKey)
    //   .valueChanges();
    return docData(
      doc(
        collection(
          doc(collection(this.firestore, 'courses'), courseKey),
          'chapters'
        ),
        chapterKey
      ).withConverter(chapterConverter)
    );
  }

  updateChapter(courseKey: string, chapterKey: string, data: Partial<Chapter>) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc<Chapter>(chapterKey)
    //   .update(data);
    return updateDoc(
      doc(
        collection(
          doc(collection(this.firestore, 'courses'), courseKey),
          'chapters'
        ),
        chapterKey
      ).withConverter(chapterConverter),
      data
    );
  }

  createChapter(courseKey: string, chapter: Chapter) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .add({
    //     ...chapter,
    //     deleted: false
    //   });
    return addDoc(
      collection(
        doc(collection(this.firestore, 'courses'), courseKey),
        'chapters'
      ).withConverter(chapterConverter),
      { ...chapter, deleted: false }
    );
  }

  removeChapter(courseKey: string, chapterKey: string) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc<Chapter>(chapterKey)
    //   .update({
    //     deleted: true
    //   });
    return updateDoc(
      doc(
        collection(
          doc(collection(this.firestore, 'courses'), courseKey),
          'chapters'
        ),
        chapterKey
      ).withConverter(chapterConverter),
      { deleted: true }
    );
  }

  listQuestions(courseKey: string, chapterKey: string) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc(chapterKey)
    //   .collection<Question>('questions', (ref) =>
    //     ref.where('deleted', '==', false)
    //   )
    //   .snapshotChanges();
    return collectionSnapshots(
      query(
        collection(
          doc(
            collection(
              doc(collection(this.firestore, 'courses'), courseKey),
              'chapters'
            ),
            chapterKey
          ),
          'questions'
        ),
        where('deleted', '==', false)
      ).withConverter(questionConverter)
    );
  }

  getQuestion(courseKey: string, chapterKey: string, questionKey: string) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc(chapterKey)
    //   .collection('questions')
    //   .doc<Question>(questionKey)
    //   .valueChanges();
    return docData(
      doc(
        collection(
          doc(
            collection(
              doc(collection(this.firestore, 'courses'), courseKey),
              'chapters'
            ),
            chapterKey
          ),
          'questions'
        ),
        questionKey
      ).withConverter(questionConverter)
    );
  }

  createQuestion(
    courseKey: string,
    chapterKey: string,
    data: Question,
    newQuestionImages: QuestionImage[]
  ) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc(chapterKey)
    //   .collection<Question>('questions')
    //   .add(data)
    //   .then((doc) => {
    //     for (let i = 0; i < newQuestionImages.length; i++) {
    //       doc.collection('images').add(newQuestionImages[i]);
    //     }
    //   });
    return addDoc(
      collection(
        doc(
          collection(
            doc(collection(this.firestore, 'courses'), courseKey),
            'chapters'
          ),
          chapterKey
        ),
        'questions'
      ).withConverter(questionConverter),
      data
    ).then((doc) => {
      for (let i = 0; i < newQuestionImages.length; i++) {
        addDoc(
          collection(doc, 'images').withConverter(questionImageConverter),
          newQuestionImages[i]
        );
      }
    });
  }

  updateQuestion(
    courseKey: string,
    chapterKey: string,
    questionKey: string,
    data: Partial<Question>,
    newQuestionImages: QuestionImage[]
  ) {
    // const theQuestion = this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc(chapterKey)
    //   .collection('questions')
    //   .doc<Question>(questionKey);
    // return theQuestion.update(data).then(() => {
    //   for (let i = 0; i < newQuestionImages.length; i++) {
    //     theQuestion.collection('images').add(newQuestionImages[i]);
    //   }
    // });
    const theQuestion = doc(
      collection(
        doc(
          collection(
            doc(collection(this.firestore, 'courses'), courseKey),
            'chapters'
          ),
          chapterKey
        ),
        'questions'
      ),
      questionKey
    ).withConverter(questionConverter);
    return updateDoc(theQuestion, data).then(() => {
      for (let i = 0; i < newQuestionImages.length; i++) {
        addDoc(
          collection(theQuestion, 'images').withConverter(
            questionImageConverter
          ),
          newQuestionImages[i]
        );
      }
    });
  }

  getQuestionImages(
    courseKey: string,
    chapterKey: string,
    questionKey: string
  ) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc(chapterKey)
    //   .collection('questions')
    //   .doc(questionKey)
    //   .collection<QuestionImage>('images')
    //   .snapshotChanges();
    return collectionSnapshots(
      collection(
        doc(
          collection(
            doc(
              collection(
                doc(collection(this.firestore, 'courses'), courseKey),
                'chapters'
              ),
              chapterKey
            ),
            'questions'
          ),
          questionKey
        ),
        'images'
      ).withConverter(questionImageConverter)
    );
  }

  uploadImage(image: File, fileName: string) {
    const id = uuidv4();
    // return this.afs.upload(`${id}/${fileName}`, image, {
    //   customMetadata: {
    //     uuid: id
    //   }
    // });
    const imageRef = ref(this.storage, `${id}/${fileName}`);
    return uploadBytes(imageRef, image, {
      customMetadata: { uuid: id }
    }).then((result) => {
      return new Promise<{ uuid: string; downloadURL: string }>((res, rej) => {
        getDownloadURL(imageRef).then((downloadURL) => {
          res({
            uuid: result.metadata.customMetadata.uuid as string,
            downloadURL
          });
        });
      });
    });
  }

  async deleteImage(
    courseKey: string,
    chapterKey: string,
    questionKey: string,
    imageKey: string,
    imageUuid: string,
    fileName: string
  ) {
    // return this.affs
    //   .collection('courses')
    //   .doc(courseKey)
    //   .collection('chapters')
    //   .doc(chapterKey)
    //   .collection('questions')
    //   .doc(questionKey)
    //   .collection('images')
    //   .doc(imageKey)
    //   .delete()
    //   .then(() => {
    //     this.afs.ref(`${imageUuid}/${fileName}`).delete().toPromise();
    //   });
    await deleteDoc(
      doc(
        collection(
          doc(
            collection(
              doc(
                collection(
                  doc(collection(this.firestore, 'courses'), courseKey),
                  'chapters'
                ),
                chapterKey
              ),
              'questions'
            ),
            questionKey
          ),
          'images'
        ),
        imageKey
      ).withConverter(questionImageConverter)
    );
    deleteObject(ref(this.storage, `${imageUuid}/${fileName}`));
  }

  deleteTempImage(imageUuid: string, fileName: string) {
    // return this.afs.ref(`${imageUuid}/${fileName}`).delete().toPromise();
    return deleteObject(ref(this.storage, `${imageUuid}/${fileName}`));
  }

  listResponse(courseKey: string, chapterKey: string) {
    return this.auth.user.pipe(
      filter((u) => u.loaded),
      map((u) => u.user.uid),
      switchMap((uid) => {
        // return this.affs
        //   .collection('users')
        //   .doc(uid)
        //   .collection('courses')
        //   .doc(courseKey)
        //   .collection('chapters')
        //   .doc(chapterKey)
        //   .collection('responses')
        //   .snapshotChanges();
        return collectionSnapshots(
          collection(
            doc(
              collection(
                doc(
                  collection(
                    doc(collection(this.firestore, 'users'), uid),
                    'courses'
                  ),
                  courseKey
                ),
                'chapters'
              ),
              chapterKey
            ),
            'responses'
          ).withConverter(userResponseConverter)
        );
      })
    );
  }

  createResponse(courseKey: string, chapterKey: string) {
    return new Promise<string>((res, rej) => {
      combineLatest([
        // this.affs
        //   .collection('courses')
        //   .doc(courseKey)
        //   .collection('chapters')
        //   .doc(chapterKey)
        //   .collection('questions')
        //   .valueChanges()
        collectionData(
          collection(
            doc(
              collection(
                doc(collection(this.firestore, 'courses'), courseKey),
                'chapters'
              ),
              chapterKey
            ),
            'questions'
          ).withConverter(questionConverter)
        ).pipe(
          map((s) => s.length),
          first()
        ),
        this.auth.user.pipe(
          filter((u) => u.loaded),
          first()
        )
      ]).subscribe(([totalScore, u]) => {
        // const ref = this.affs
        //   .collection('users')
        //   .doc(u.user.uid)
        //   .collection('courses')
        //   .doc(courseKey)
        //   .collection('chapters')
        //   .doc(chapterKey)
        //   .collection('responses')
        //   .add({
        //     createdAt: serverTimestamp(),
        //     updatedAt: serverTimestamp(),
        //     score: 0,
        //     done: 0,
        //     totalScore
        //   });
        const ref = addDoc(
          collection(
            doc(
              collection(
                doc(
                  collection(
                    doc(collection(this.firestore, 'users'), u.user.uid),
                    'courses'
                  ),
                  courseKey
                ),
                'chapters'
              ),
              chapterKey
            ),
            'responses'
          ).withConverter(userResponseConverter),
          {
            createdAt: serverTimestamp(),
            updatedAt: serverTimestamp(),
            score: 0,
            done: 0,
            totalScore
          }
        );
        ref.then((r) => {
          res(r.id);
        });
      });
    });
  }

  getResponse(courseKey: string, chapterKey: string, responseKey: string) {
    return this.auth.user.pipe(
      filter((u) => u.loaded),
      switchMap((u) => {
        // return this.affs
        //   .collection('users')
        //   .doc(u.user.uid)
        //   .collection('courses')
        //   .doc(courseKey)
        //   .collection('chapters')
        //   .doc(chapterKey)
        //   .collection('responses')
        //   .doc<UserResponse>(responseKey)
        //   .valueChanges();
        return docData(
          doc(
            collection(
              doc(
                collection(
                  doc(
                    collection(
                      doc(collection(this.firestore, 'users'), u.user.uid),
                      'courses'
                    ),
                    courseKey
                  ),
                  'chapters'
                ),
                chapterKey
              ),
              'responses'
            ),
            responseKey
          ).withConverter(userResponseConverter)
        );
      })
    );
  }

  getQuestionResponses(
    courseKey: string,
    chapterKey: string,
    responseKey: string
  ) {
    return this.auth.user.pipe(
      filter((u) => u.loaded),
      switchMap((u) => {
        // return this.affs
        //   .collection('users')
        //   .doc(u.user.uid)
        //   .collection('courses')
        //   .doc(courseKey)
        //   .collection('chapters')
        //   .doc(chapterKey)
        //   .collection('responses')
        //   .doc(responseKey)
        //   .collection('questionResponses')
        //   .snapshotChanges();
        return collectionSnapshots(
          collection(
            doc(
              collection(
                doc(
                  collection(
                    doc(
                      collection(
                        doc(collection(this.firestore, 'users'), u.user.uid),
                        'courses'
                      ),
                      courseKey
                    ),
                    'chapters'
                  ),
                  chapterKey
                ),
                'responses'
              ),
              responseKey
            ),
            'questionResponses'
          ).withConverter(questionResponseConverter)
        );
      })
    );
  }

  submitAnswer(
    courseKey: string,
    chapterKey: string,
    responseKey: string,
    questionKey: string,
    answer: any
  ) {
    this.auth.user
      .pipe(
        filter((u) => u.loaded),
        first()
      )
      .subscribe((u) => {
        const submitAnswer = httpsCallable(this.functions, 'submitAnswer');
        submitAnswer({
          courseKey,
          chapterKey,
          responseKey,
          questionKey,
          answer,
          uid: u.user.uid
        });
      });
  }

  createReport(
    courseKey: string,
    chapterKey: string,
    questionKey: string,
    report: string
  ) {
    this.auth.user
      .pipe(
        filter((u) => u.loaded),
        first()
      )
      .subscribe((u) => {
        // this.affs.collection('reports').add({
        //   courseKey,
        //   chapterKey,
        //   questionKey,
        //   author: {
        //     name: u.user.displayName,
        //     email: u.user.email,
        //     uid: u.user.uid
        //   },
        //   report
        // });
        addDoc(collection(this.firestore, 'reports'), {
          courseKey,
          chapterKey,
          questionKey,
          author: {
            name: u.user.displayName,
            email: u.user.email,
            uid: u.user.uid
          },
          report
        });
      });
  }

  listReport() {
    // return this.affs.collection<any>('reports').valueChanges();
    return collectionData(collection(this.firestore, 'reports'));
  }
}

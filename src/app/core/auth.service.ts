import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import firebase from 'firebase/compat/app';
import { Observable, of } from 'rxjs';
import { map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { User } from '../shared/user';

@Injectable()
export class AuthService {
  user: Observable<User>;
  roles: Observable<string[]>;
  constructor(
    private afa: AngularFireAuth,
    private router: Router,
    private affs: AngularFirestore
  ) {
    this.user = this.afa.authState.pipe(
      map((user) => {
        return {
          loaded: true,
          user
        };
      }),
      tap((bundle) => {
        const u = bundle.user;
        if (u) {
          this.affs.collection('users').doc(u.uid).set({
            email: u.email,
            displayName: u.displayName
          });
        }
      }),
      startWith({
        loaded: false,
        user: null
      }),
      shareReplay(1)
    );
    this.roles = this.afa.authState.pipe(
      switchMap((user) => {
        if (user) {
          return this.affs
            .collection('roles')
            .doc<{ roles: string[] }>(user.uid)
            .valueChanges()
            .pipe(
              map((data) => {
                if (data) {
                  return data.roles;
                } else {
                  return [];
                }
              })
            );
        } else {
          return of([]);
        }
      }),
      shareReplay(1)
    );
  }

  signIn() {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.setCustomParameters({
      hd: 'docchula.com',
      prompt: 'select_account'
    });
    this.afa.signInWithRedirect(provider);
  }

  signOut() {
    this.afa.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }
}

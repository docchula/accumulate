import { TestBed, inject } from '@angular/core/testing';

import { QuestionService } from './question.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';

class AngularFirestoreStub {}

describe('QuestionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        QuestionService,
        {
          provide: AngularFirestore,
          useClass: AngularFirestoreStub
        }
      ]
    });
  });

  // TODO: proper tests should be created
});

import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { of } from 'rxjs';

import { AuthService } from './auth.service';

class RouterStub {
  navigate(params: any) {}
}

class AngularFireAuthStub {
  authState = of({});
}

class AngularFirestoreStub {}

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        {
          provide: Router,
          useClass: RouterStub
        },
        {
          provide: AngularFireAuth,
          useClass: AngularFireAuthStub
        },
        {
          provide: AngularFirestore,
          useClass: AngularFirestoreStub
        }
      ]
    });
  });

  // TODO: proper tests should be created

  it(
    'should be created',
    inject([AuthService], (service: AuthService) => {
      expect(service).toBeTruthy();
    })
  );
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from './role.guard';
import { UserGuard } from './user.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then((m) => m.MainModule),
    canLoad: [UserGuard]
  },
  {
    path: 'manage',
    loadChildren: () =>
      import('./management/management.module').then((m) => m.ManagementModule),
    canLoad: [RoleGuard],
    data: {
      allowedRoles: ['admin', 'editor']
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
  providers: [RoleGuard, UserGuard]
})
export class AppRoutingModule {}

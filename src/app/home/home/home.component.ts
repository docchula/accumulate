import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../core/auth.service';
import { User } from '../../shared/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'ac-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  user$: Observable<User>;

  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.user$ = this.auth.user;
  }

  signIn() {
    this.auth.signIn();
  }
}

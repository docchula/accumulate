import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { of } from 'rxjs';

import { AuthService } from '../../core/auth.service';
import { TestingModule } from '../../testing/testing.module';
import { HomeComponent } from './home.component';

class AuthServiceStub {
  user = of({ loaded: false, user: {} });
}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [HomeComponent],
        providers: [
          {
            provide: AuthService,
            useClass: AuthServiceStub
          }
        ],
        imports: [TestingModule]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: proper tests should be created

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

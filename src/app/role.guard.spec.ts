import { TestBed, inject, waitForAsync } from '@angular/core/testing';

import { RoleGuard } from './role.guard';
import { AuthService } from './core/auth.service';

class AuthServiceStub {}

describe('RoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RoleGuard,
        {
          provide: AuthService,
          useClass: AuthServiceStub
        }
      ]
    });
  });

  // TODO: proper tests should be created

  it(
    'should ...',
    inject([RoleGuard], (guard: RoleGuard) => {
      expect(guard).toBeTruthy();
    })
  );
});

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MarkedModule } from '../shared/marked/marked.module';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { CourseListComponent } from './course-list/course-list.component';
import { CoursePrintingComponent } from './course-printing/course-printing.component';
import { MainRoutingModule } from './main-routing.module';
import { McqQuestionComponent } from './response-editor/mcq-question/mcq-question.component';
import { ResponseEditorComponent } from './response-editor/response-editor.component';
import { ResponseListComponent } from './response-list/response-list.component';

@NgModule({
  imports: [CommonModule, MainRoutingModule, MarkedModule, ReactiveFormsModule],
  declarations: [
    CourseListComponent,
    ChapterListComponent,
    ResponseListComponent,
    ResponseEditorComponent,
    McqQuestionComponent,
    CoursePrintingComponent
  ]
})
export class MainModule {}

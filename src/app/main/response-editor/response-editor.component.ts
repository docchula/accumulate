import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { QuestionService } from '../../core/question.service';
import { Chapter } from '../../shared/chapter';
import { Course } from '../../shared/course';
import { Question } from '../../shared/question';
import { QuestionResponse } from '../../shared/question-response';
import { UserResponse } from '../../shared/user-response';

@Component({
  selector: 'ac-response-editor',
  templateUrl: './response-editor.component.html',
  styleUrls: ['./response-editor.component.scss']
})
export class ResponseEditorComponent implements OnInit {
  courseKey$: Observable<string>;
  chapterKey$: Observable<string>;
  course$: Observable<Course>;
  chapter$: Observable<Chapter>;
  questions$: Observable<(Question & { $key: string })[]>;
  responseKey$: Observable<string>;
  response$: Observable<UserResponse>;
  questionResponses$: Observable<{ [key: string]: QuestionResponse }>;

  constructor(
    private route: ActivatedRoute,
    private question: QuestionService
  ) {}

  ngOnInit() {
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.chapterKey$ = this.route.data.pipe(map((data) => data.chapterKey));
    this.course$ = this.route.data.pipe(map((data) => data.course));
    this.chapter$ = this.route.data.pipe(map((data) => data.chapter));
    this.responseKey$ = this.route.paramMap.pipe(
      map((paramMap) => paramMap.get('responseKey'))
    );
    this.questions$ = combineLatest(this.courseKey$, this.chapterKey$).pipe(
      switchMap((param) => {
        return this.question.listQuestions(param[0], param[1]);
      }),
      map((data) => {
        return data.map((q) => {
          return {
            ...q.data(),
            $key: q.id
          };
        });
      })
    );
    this.response$ = combineLatest(
      this.courseKey$,
      this.chapterKey$,
      this.responseKey$
    ).pipe(
      switchMap(([courseKey, chapterKey, responseKey]) => {
        return this.question.getResponse(courseKey, chapterKey, responseKey);
      })
    );
    this.questionResponses$ = combineLatest(
      this.courseKey$,
      this.chapterKey$,
      this.responseKey$
    ).pipe(
      switchMap(([courseKey, chapterKey, responseKey]) => {
        return this.question.getQuestionResponses(
          courseKey,
          chapterKey,
          responseKey
        );
      }),
      map((snaps) => {
        return snaps.reduce((prev, curr, index, arr) => {
          return {
            ...prev,
            [curr.id]: curr.data()
          };
        }, {});
      }),
      startWith({}),
      tap((c) => console.log(c)),
      shareReplay(1)
    );
  }
}

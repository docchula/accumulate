import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { QuestionResponse } from '../../../shared/question-response';
import { McqQuestion } from '../../../shared/question-types/mcq-question';
import { QuestionService } from '../../../core/question.service';

@Component({
  selector: 'ac-mcq-question',
  templateUrl: './mcq-question.component.html',
  styleUrls: ['./mcq-question.component.scss']
})
export class McqQuestionComponent implements OnInit {
  @Input() question: McqQuestion;
  @Input() questionResponse: QuestionResponse;
  @Input() courseKey: string;
  @Input() chapterKey: string;
  @Input() responseKey: string;

  reporting = false;
  reportingForm: FormControl;

  answerForm: FormControl;

  constructor(private questionService: QuestionService) {}

  ngOnInit() {
    this.answerForm = new FormControl(null, Validators.required);
    this.reportingForm = new FormControl('', Validators.required);
  }

  submit() {
    if (this.answerForm.valid) {
      this.submitAnswer(this.answerForm.value);
    }
  }

  iDontKnow() {
    this.submitAnswer(-1);
  }

  submitAnswer(value: number) {
    this.questionService.submitAnswer(
      this.courseKey,
      this.chapterKey,
      this.responseKey,
      this.question.$key,
      value
    );
  }

  report() {
    this.reporting = true;
  }

  sendReport() {
    if (this.reportingForm.valid) {
      this.questionService.createReport(
        this.courseKey,
        this.chapterKey,
        this.question.$key,
        this.reportingForm.value
      );
      this.reporting = false;
    }
  }
}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResponseEditorComponent } from './response-editor.component';

describe('ResponseEditorComponent', () => {
  let component: ResponseEditorComponent;
  let fixture: ComponentFixture<ResponseEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponseEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: proper tests should be created
});

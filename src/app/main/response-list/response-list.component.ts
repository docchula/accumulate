import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { QuestionService } from '../../core/question.service';
import { Chapter } from '../../shared/chapter';
import { Course } from '../../shared/course';
import { UserResponse } from '../../shared/user-response';

@Component({
  selector: 'ac-response-list',
  templateUrl: './response-list.component.html',
  styleUrls: ['./response-list.component.scss']
})
export class ResponseListComponent implements OnInit {
  responses$: Observable<(UserResponse & { $key: string })[]>;
  courseKey$: Observable<string>;
  chapterKey$: Observable<string>;
  course$: Observable<Course>;
  chapter$: Observable<Chapter>;

  constructor(
    private question: QuestionService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.course$ = this.route.data.pipe(map((data) => data.course));
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.chapter$ = this.route.data.pipe(map((data) => data.chapter));
    this.chapterKey$ = this.route.data.pipe(map((data) => data.chapterKey));
    this.responses$ = combineLatest([this.courseKey$, this.chapterKey$]).pipe(
      switchMap((bundle) => this.question.listResponse(bundle[0], bundle[1])),
      map((snaps) => {
        return snaps.map((snap) => {
          return {
            ...snap.data(),
            $key: snap.id
          };
        });
      })
    );
  }

  newResponse() {
    combineLatest(this.courseKey$, this.chapterKey$)
      .pipe(first())
      .subscribe((bundle) => {
        this.question.createResponse(bundle[0], bundle[1]).then((id) => {
          this.router.navigate([id], { relativeTo: this.route });
        });
      });
  }
}

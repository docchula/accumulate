import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../core/question.service';
import { Observable } from 'rxjs';
import { Chapter } from '../../shared/chapter';
import { Course } from '../../shared/course';
import { map, switchMap, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'ac-chapter-list',
  templateUrl: './chapter-list.component.html',
  styleUrls: ['./chapter-list.component.scss']
})
export class ChapterListComponent implements OnInit {
  chapters$: Observable<(Chapter & { $key: string })[]>;
  course$: Observable<Course>;
  courseKey$: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private question: QuestionService
  ) {}

  ngOnInit() {
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.course$ = this.route.data.pipe(map((data) => data.course));
    this.chapters$ = this.courseKey$.pipe(
      switchMap((courseKey) => {
        return this.question.listChapter(courseKey);
      }),
      map((data) => {
        return data.map((snap) => {
          return {
            // ...(snap.payload.doc.data() as Chapter),
            // $key: snap.payload.doc.id
            ...snap.data(),
            $key: snap.id
          };
        });
      }),
      shareReplay(1)
    );
  }
}

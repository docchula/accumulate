import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChapterKeyResolver } from '../shared/chapter-key-resolver';
import { ChapterResolver } from '../shared/chapter-resolver';
import { CourseKeyResolver } from '../shared/course-key-resolver';
import { CourseResolver } from '../shared/course-resolver';
import { QuestionKeyResolver } from '../shared/question-key-resolver';
import { QuestionResolver } from '../shared/question-resolver';
import { UserGuard } from '../user.guard';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { CourseListComponent } from './course-list/course-list.component';
import { CoursePrintingComponent } from './course-printing/course-printing.component';
import { ResponseEditorComponent } from './response-editor/response-editor.component';
import { ResponseListComponent } from './response-list/response-list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'courses'
  },
  {
    path: '',
    canActivate: [UserGuard],
    children: [
      {
        path: 'courses',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: CourseListComponent
          },
          {
            path: ':courseKey',
            resolve: {
              courseKey: CourseKeyResolver,
              course: CourseResolver
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                component: ChapterListComponent
              },
              {
                path: 'print',
                component: CoursePrintingComponent
              },
              {
                path: ':chapterKey',
                resolve: {
                  chapterKey: ChapterKeyResolver,
                  chapter: ChapterResolver
                },
                children: [
                  {
                    path: '',
                    pathMatch: 'full',
                    component: ResponseListComponent
                  },
                  {
                    path: ':responseKey',
                    component: ResponseEditorComponent
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UserGuard,
    CourseResolver,
    ChapterResolver,
    CourseKeyResolver,
    ChapterKeyResolver,
    QuestionResolver,
    QuestionKeyResolver
  ]
})
export class MainRoutingModule {}

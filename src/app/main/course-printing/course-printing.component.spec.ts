import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CoursePrintingComponent } from './course-printing.component';

describe('CoursePrintingComponent', () => {
  let component: CoursePrintingComponent;
  let fixture: ComponentFixture<CoursePrintingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursePrintingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursePrintingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: proper tests should be created
});

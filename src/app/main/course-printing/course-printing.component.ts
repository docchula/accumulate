import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../core/question.service';
import { Observable, combineLatest } from 'rxjs';
import { map, first, switchMap, shareReplay } from 'rxjs/operators';
import { Chapter } from '../../shared/chapter';
import { McqQuestion } from '../../shared/question-types/mcq-question';
import { Course } from '../../shared/course';

@Component({
  selector: 'ac-course-printing',
  templateUrl: './course-printing.component.html',
  styleUrls: ['./course-printing.component.scss']
})
export class CoursePrintingComponent implements OnInit {
  courseKey$: Observable<string>;
  chapters$: Observable<(Chapter & { $key: string })[]>;
  questions$: Observable<
    { chapter: Chapter & { $key: string }; questions: McqQuestion[] }[]
  >;
  course$: Observable<Course>;

  constructor(
    private route: ActivatedRoute,
    private question: QuestionService
  ) {}

  ngOnInit() {
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.course$ = this.route.data.pipe(map((data) => data.course));
    this.chapters$ = this.courseKey$.pipe(
      switchMap((courseKey) => {
        return this.question.listChapter(courseKey);
      }),
      map((data) => {
        return data.map((snap) => {
          return {
            ...snap.data(),
            $key: snap.id
          };
        });
      }),
      shareReplay(1)
    );
    this.questions$ = this.courseKey$.pipe(
      first(),
      switchMap((courseKey) => {
        return this.chapters$.pipe(
          switchMap((chapters) => {
            return combineLatest(
              chapters.map((chapter) => {
                return this.question
                  .listQuestions(courseKey, chapter.$key)
                  .pipe(
                    map((questions) => {
                      return {
                        chapter,
                        questions: questions.map((question) => {
                          return question.data();
                        })
                      };
                    })
                  );
              })
            );
          })
        );
      })
    );
  }
}

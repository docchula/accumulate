import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from '../../shared/course';
import { QuestionService } from '../../core/question.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ac-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {
  courses$: Observable<(Course & { $key: string })[]>;

  constructor(private question: QuestionService) {}

  ngOnInit() {
    this.courses$ = this.question.listCourses().pipe(
      map((snaps) => {
        return snaps.map((snap) => {
          return {
            $key: snap.id,
            ...snap.data()
          };
        });
      })
    );
  }
}

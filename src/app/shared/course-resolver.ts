import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { first } from 'rxjs/operators';
import { QuestionService } from '../core/question.service';
import { Course } from './course';

@Injectable()
export class CourseResolver implements Resolve<Course> {
  constructor(private question: QuestionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.question
      .getCourse(route.paramMap.get('courseKey'))
      .pipe(first());
  }
}

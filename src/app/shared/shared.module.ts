import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MarkedModule } from './marked/marked.module';
import { MarkedPipe } from './marked/marked.pipe';
import { NavbarComponent } from './navbar/navbar.component';
import { RoleDirective } from './role.directive';

@NgModule({
  imports: [CommonModule, RouterModule, MarkedModule.forRoot()],
  exports: [NavbarComponent, RoleDirective, MarkedPipe],
  declarations: [NavbarComponent, RoleDirective]
})
export class SharedModule {}

export interface QuestionImage {
  fileName: string;
  imageUrl: string;
  uuid: string;
}

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { first } from 'rxjs/operators';
import { QuestionService } from '../core/question.service';
import { Question } from './question';

@Injectable()
export class QuestionResolver implements Resolve<Question> {
  constructor(private question: QuestionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.question
      .getQuestion(
        route.paramMap.get('courseKey'),
        route.paramMap.get('chapterKey'),
        route.paramMap.get('questionKey')
      )
      .pipe(first());
  }
}

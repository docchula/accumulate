export interface Chapter {
  name: string;
  deleted: boolean;
}

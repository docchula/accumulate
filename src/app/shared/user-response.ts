export interface UserResponse {
  createdAt: Date;
  updatedAt: Date;
  score: number;
  done: number;
  totalScore: number;
}

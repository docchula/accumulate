import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { first } from 'rxjs/operators';
import { QuestionService } from '../core/question.service';
import { Chapter } from './chapter';

@Injectable()
export class ChapterResolver implements Resolve<Chapter> {
  constructor(private question: QuestionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.question
      .getChapter(
        route.paramMap.get('courseKey'),
        route.paramMap.get('chapterKey')
      )
      .pipe(first());
  }
}

import { User as fbUser } from '@angular/fire/auth';

export interface User {
  loaded: boolean;
  user: fbUser;
}

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/auth.service';
import { User } from '../user';

@Component({
  selector: 'ac-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  burgerStatus = false;
  user$: Observable<User>;

  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.user$ = this.auth.user;
  }

  toggleBurger() {
    this.burgerStatus = !this.burgerStatus;
  }

  signIn() {
    this.auth.signIn();
  }

  signOut() {
    this.auth.signOut();
  }
}

import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';

import { AuthService } from '../../core/auth.service';
import { NavbarComponent } from './navbar.component';

class AuthServiceStub {
  authState = new Subject();
}

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let burger: DebugElement;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [NavbarComponent],
        providers: [
          {
            provide: AuthService,
            useClass: AuthServiceStub
          }
        ]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    burger = fixture.debugElement.query(By.css('div.navbar-burger'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have burgerStatus of false', () => {
    expect(component.burgerStatus).toBe(false);
  });

  it('should toggle burgerStatus when toggleBurger is called', () => {
    component.toggleBurger();
    expect(component.burgerStatus).toBe(true);
    component.toggleBurger();
    expect(component.burgerStatus).toBe(false);
  });

  it('should call toggleBurger when burger is clicked', () => {
    spyOn(component, 'toggleBurger');
    burger.nativeElement.click();
    expect(component.toggleBurger).toHaveBeenCalledTimes(1);
  });

  it('should correctly assign class to burger', () => {
    expect(burger.classes['is-active']).toBeFalsy();
    component.burgerStatus = true;
    fixture.detectChanges();
    expect(burger.classes['is-active']).toBeTruthy();
  });
});

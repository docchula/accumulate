// export interface Question {
//   type: string;
//   deleted: boolean;
//   $key: string;
// }
import { McqQuestion } from './question-types/mcq-question';
export type Question = McqQuestion;

export interface BaseQuestion {
  deleted: boolean;
  $key: string;
}

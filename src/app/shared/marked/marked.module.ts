import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MarkedPipe } from './marked.pipe';
import { MarkedService } from './marked.service';

@NgModule({
  imports: [CommonModule],
  declarations: [MarkedPipe],
  exports: [MarkedPipe]
})
export class MarkedModule {
  static forRoot(): ModuleWithProviders<MarkedModule> {
    return {
      ngModule: MarkedModule,
      providers: [MarkedService]
    };
  }
}

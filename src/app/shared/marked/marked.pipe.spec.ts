import { MarkedPipe } from './marked.pipe';
import { MarkedService } from './marked.service';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

class MarkedServiceStub {
  compile(text: string, options: marked.MarkedOptions): SafeHtml {
    throw new Error('Method not implemented.');
  }
}

describe('MarkedPipe', () => {
  it('create an instance', () => {
    const pipe = new MarkedPipe({}, (new MarkedServiceStub()) as any);
    expect(pipe).toBeTruthy();
  });

  it('should call MarkedService correctly', () => {
    const marked = (new MarkedServiceStub()) as any;
    const pipe = new MarkedPipe({}, marked);
    spyOn(marked, 'compile');
    pipe.transform('this is a string', false);
    expect(marked.compile).toHaveBeenCalledWith('this is a string', {});
  });
});

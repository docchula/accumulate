import { InjectionToken } from '@angular/core';

export const MARKED_OPTIONS = new InjectionToken<any>('MARKED_OPTIONS');
export const MARKED = new InjectionToken<Function>('MARKED');

import { Inject, Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as _marked from 'marked';
import { MARKED } from './di';

@Injectable()
export class MarkedService {
  renderer = new _marked.Renderer();
  defaultOpions: _marked.MarkedOptions = {
    renderer: this.renderer
  };

  constructor(
    private sanitizer: DomSanitizer,
    @Inject(MARKED) private marked: any
  ) {}

  compile(
    text: string,
    options: _marked.MarkedOptions,
    inline: boolean = false
  ) {
    if (text) {
      if (!inline) {
        return this.sanitizer.bypassSecurityTrustHtml(
          this.marked(text, { ...this.defaultOpions, ...options })
        );
      } else {
        return this.sanitizer.bypassSecurityTrustHtml(
          this.marked.inlineLexer(text, [], {
            ...this.defaultOpions,
            ...options
          })
        );
      }
    } else {
      return this.sanitizer.bypassSecurityTrustHtml('');
    }
  }
}

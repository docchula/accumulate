import { Inject, Optional, Pipe, PipeTransform } from '@angular/core';
import * as marked from 'marked';
import { MARKED_OPTIONS } from './di';
import { MarkedService } from './marked.service';

@Pipe({
  name: 'marked'
})
export class MarkedPipe implements PipeTransform {
  constructor(
    @Inject(MARKED_OPTIONS)
    @Optional()
    private markedOptions: marked.MarkedOptions,
    private markedService: MarkedService
  ) {}

  transform(value: string, inline: boolean, args?: any) {
    if (inline) {
      return this.markedService.compile(value, this.markedOptions, true);
    } else {
      return this.markedService.compile(value, this.markedOptions);
    }
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { MarkedService } from './marked.service';
import { MARKED } from './di';
import * as marked from 'marked';
import { DomSanitizer } from '@angular/platform-browser';

class DomSanitizerStub {
  bypassSecurityTrustHtml(text: string) {
    return `SAFEHTML(${text})`;
  }
}

describe('MarkedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MarkedService,
        {
          provide: MARKED,
          useValue: (text: string, options: marked.MarkedOptions) => {
            return `MARKED(${text})`;
          }
        },
        {
          provide: DomSanitizer,
          useClass: DomSanitizerStub
        }
      ]
    });
  });

  it(
    'should be created',
    inject([MarkedService], (service: MarkedService) => {
      expect(service).toBeTruthy();
    })
  );

  it(
    'should call marked library',
    inject([MarkedService], (service: MarkedService) => {
      const markedText = service.compile('test', {});
      expect(markedText).toBe('SAFEHTML(MARKED(test))');
    })
  );

  it(
    'should not call marked library for falsy value',
    inject([MarkedService], (service: MarkedService) => {
      const markedText = service.compile(null, {});
      expect(markedText).toBe('SAFEHTML()');
    })
  );
});

export interface Course {
  code: string;
  nameTH: string;
  nameEN: string;
  abbr: string;
  deleted: boolean;
}

import {
  Directive,
  OnInit,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { AuthService } from '../core/auth.service';

@Directive({
  selector: '[acRole]',
  exportAs: 'roles'
})
export class RoleDirective implements OnInit {
  constructor(
    private auth: AuthService,
    private vcRef: ViewContainerRef,
    private templateRef: TemplateRef<any>
  ) {}

  ngOnInit() {
    this.vcRef.clear();
    this.vcRef.createEmbeddedView(this.templateRef, {
      $implicit: this.auth.roles
    });
  }
}

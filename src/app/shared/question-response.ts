export interface QuestionResponse {
  answer: any;
  graded: boolean;
  correct: boolean;
}

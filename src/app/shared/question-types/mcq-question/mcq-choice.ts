export interface McqChoice {
  choiceText: string;
  correct: boolean;
}

import { BaseQuestion } from '../question';
import { McqChoice } from './mcq-question/mcq-choice';

export interface McqQuestion extends BaseQuestion {
  type: 'mcq';
  choices: McqChoice[];
  questionText: string;
  keyText: string;
}

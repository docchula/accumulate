import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { AuthService } from './core/auth.service';

@Injectable()
export class UserGuard implements CanActivate, CanLoad {
  constructor(private auth: AuthService) {}

  isLoggedIn() {
    return this.auth.user.pipe(
      first(),
      map((u) => {
        if (u) {
          return true;
        } else {
          return false;
        }
      })
    );
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.isLoggedIn();
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedIn();
  }
}

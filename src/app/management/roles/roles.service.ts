import { Injectable } from '@angular/core';
import {
  collection,
  collectionSnapshots,
  deleteDoc,
  doc,
  docData,
  Firestore,
  setDoc,
  updateDoc
} from '@angular/fire/firestore';
import { first } from 'rxjs/operators';

@Injectable()
export class RolesService {
  constructor(private firestore: Firestore) {}

  getRoles() {
    // return this.affs.collection('roles').snapshotChanges();
    return collectionSnapshots(collection(this.firestore, 'roles'));
  }

  updateData(uid: string) {
    // this.affs
    //   .collection('users')
    //   .doc(uid)
    //   .valueChanges()
    docData(doc(collection(this.firestore, 'users'), uid))
      .pipe(first())
      .subscribe((v) => {
        if (v) {
          // this.affs.collection('roles').doc(uid).update(v);
          updateDoc(doc(collection(this.firestore, 'roles'), uid), v);
        }
      });
  }

  deleteRole(uid: string) {
    // return this.affs.collection('roles').doc(uid).delete();
    return deleteDoc(doc(collection(this.firestore, 'roles'), uid));
  }

  updateRole(uid: string, roles: string[]) {
    // return this.affs.collection('roles').doc(uid).update({
    //   roles
    // });
    return updateDoc(doc(collection(this.firestore, 'roles'), uid), { roles });
  }

  createRole(uid: string, roles: string[]) {
    // return this.affs.collection('roles').doc(uid).set({
    //   roles
    // });
    return setDoc(doc(collection(this.firestore, 'roles'), uid), {
      roles
    });
  }

  getUsers() {
    // return this.affs.collection('users').snapshotChanges();
    return collectionSnapshots(collection(this.firestore, 'users'));
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../../role.guard';
import { RolesEditorComponent } from './roles-editor/roles-editor.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RoleGuard],
    data: {
      allowedRoles: ['admin']
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: RolesEditorComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule {}

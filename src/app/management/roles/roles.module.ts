import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RolesEditorComponent } from './roles-editor/roles-editor.component';
import { RolesRoutingModule } from './roles-routing.module';
import { RolesService } from './roles.service';

@NgModule({
  imports: [CommonModule, RolesRoutingModule, ReactiveFormsModule],
  declarations: [RolesEditorComponent],
  providers: [RolesService]
})
export class RolesModule {}

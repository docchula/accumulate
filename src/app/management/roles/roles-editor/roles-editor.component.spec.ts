import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RolesEditorComponent } from './roles-editor.component';

describe('RolesEditorComponent', () => {
  let component: RolesEditorComponent;
  let fixture: ComponentFixture<RolesEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: proper tests should be created
});

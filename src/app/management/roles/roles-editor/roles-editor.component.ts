import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RolesService } from '../roles.service';

@Component({
  selector: 'ac-roles-editor',
  templateUrl: './roles-editor.component.html',
  styleUrls: ['./roles-editor.component.scss']
})
export class RolesEditorComponent implements OnInit {
  roles$: Observable<any>;
  users$: Observable<any[]>;
  newRoleForm: FormGroup;
  editRoleForm: FormGroup;
  editingRoleUid: string;

  constructor(private roles: RolesService) {}

  ngOnInit() {
    this.roles$ = this.roles.getRoles().pipe(
      map((snaps) => {
        return snaps.map((snap) => {
          const res: {
            $uid: string;
            email: string;
            displayName: string;
            roles: string[];
          } = {
            $uid: snap.id,
            ...(snap.data() as any)
          };
          if (!res.email) {
            this.roles.updateData(res.$uid);
          }
          return res;
        });
      })
    );
    this.newRoleForm = new FormGroup({
      uid: new FormControl(''),
      roles: new FormControl('')
    });
    this.editRoleForm = new FormGroup({
      roles: new FormControl('')
    });
    this.editingRoleUid = null;
  }

  editRole(role: {
    $uid: string;
    displayName: string;
    email: string;
    roles: string[];
  }) {
    this.editRoleForm.setValue({
      roles: role.roles.join(',')
    });
    this.editingRoleUid = role.$uid;
  }

  deleteRole(role: {
    $uid: string;
    displayName: string;
    email: string;
    roles: string[];
  }) {
    this.roles.deleteRole(role.$uid);
  }

  getRolesFromString(text: string) {
    return text.split(',').map((a) => a.trim());
  }

  updateRole() {
    const roles = this.getRolesFromString(this.editRoleForm.get('roles').value);
    this.roles.updateRole(this.editingRoleUid, roles).then(() => {
      this.editRoleForm.reset();
      this.editingRoleUid = null;
    });
  }

  createRole() {
    const roles = this.getRolesFromString(this.newRoleForm.get('roles').value);
    this.roles.createRole(this.newRoleForm.get('uid').value, roles).then(() => {
      this.newRoleForm.reset();
    });
  }
}

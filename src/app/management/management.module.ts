import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home/home.component';
import { ManagementRoutingModule } from './management-routing.module';

@NgModule({
  imports: [CommonModule, ManagementRoutingModule, SharedModule],
  declarations: [HomeComponent]
})
export class ManagementModule {}

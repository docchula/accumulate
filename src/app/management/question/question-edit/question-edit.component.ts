import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClipboardService } from 'ngx-clipboard';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { QuestionService } from '../../../core/question.service';
import { Question } from '../../../shared/question';
import { QuestionImage } from '../../../shared/question-image';
import { questionTypes } from '../../../shared/question-types';

@Component({
  selector: 'ac-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.scss']
})
export class QuestionEditComponent implements OnInit {
  questionTypes = questionTypes;
  question$: Observable<Question>;
  courseKey$: Observable<string>;
  chapterKey$: Observable<string>;
  questionKey$: Observable<string>;
  questionImages$: Observable<(QuestionImage & { $key: string })[]>;
  editMode$: Observable<string>;
  newQuestionImages: QuestionImage[];
  selectedFileName: string;
  selectedFile: File;
  fileValid: boolean;
  uploading: boolean;

  constructor(
    private route: ActivatedRoute,
    private question: QuestionService,
    private router: Router,
    private clipboard: ClipboardService
  ) {}

  ngOnInit() {
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.chapterKey$ = this.route.data.pipe(map((data) => data.chapterKey));
    this.editMode$ = this.route.data.pipe(map((data) => data.editMode));
    this.editMode$.pipe(first()).subscribe((em) => {
      if (em === 'edit') {
        this.question$ = this.route.data.pipe(map((data) => data.question));
        this.questionKey$ = this.route.data.pipe(
          map((data) => data.questionKey)
        );
      } else {
        this.question$ = of({
          type: 'mcq',
          deleted: false,
          $key: null,
          choices: [],
          questionText: '',
          keyText: ''
        });
        this.questionKey$ = of(null);
      }
    });
    this.questionImages$ = combineLatest(
      this.courseKey$,
      this.chapterKey$,
      this.questionKey$
    ).pipe(
      switchMap((params) => {
        if (params[2] !== null) {
          return this.question
            .getQuestionImages(params[0], params[1], params[2])
            .pipe(
              map((snaps) => {
                return snaps.map((snap) => {
                  return {
                    ...snap.data(),
                    $key: snap.id
                  };
                });
              })
            );
        } else {
          return of([]);
        }
      })
    );
    this.newQuestionImages = [];
    this.selectedFileName = 'ยังไม่ได้เลือกรูป';
    this.fileValid = false;
  }

  update(data: Question) {
    combineLatest(this.courseKey$, this.chapterKey$, this.questionKey$)
      .pipe(first())
      .subscribe((params) => {
        if (params[2] !== null) {
          this.question
            .updateQuestion(
              params[0],
              params[1],
              params[2],
              data,
              this.newQuestionImages
            )
            .then(() => {
              this.router.navigate(['../..'], { relativeTo: this.route });
            });
        } else {
          this.question
            .createQuestion(params[0], params[1], data, this.newQuestionImages)
            .then(() => {
              this.router.navigate(['..'], { relativeTo: this.route });
            });
        }
      });
  }

  onFileSelect(e: Event) {
    console.log(e);
    const input = e.target as HTMLInputElement;
    if (
      input.files[0].type === 'image/png' ||
      input.files[0].type === 'image/jpeg'
    ) {
      this.selectedFileName = input.files[0].name;
      this.selectedFile = input.files[0];
      this.fileValid = true;
    } else {
      this.selectedFileName = 'กรุณาเลือกรูปภาพ';
      this.fileValid = false;
    }
  }

  upload() {
    if (this.fileValid) {
      this.uploading = true;
      this.question
        .uploadImage(this.selectedFile, this.selectedFileName)
        .then((res) => {
          this.newQuestionImages.push({
            fileName: this.selectedFileName,
            uuid: res.uuid,
            imageUrl: res.downloadURL
          });
          this.selectedFile = null;
          this.selectedFileName = 'ยังไม่ได้เลือกไฟล์';
          this.fileValid = false;
          this.uploading = false;
        });
    }
  }

  copyUrl(image: QuestionImage & { $key: string }) {
    this.clipboard.copyFromContent(image.imageUrl);
    alert('Copied');
  }

  deleteImage(image: QuestionImage & { $key: string }) {
    if (image.$key) {
      combineLatest(this.courseKey$, this.chapterKey$, this.questionKey$)
        .pipe(first())
        .subscribe((params) => {
          this.question.deleteImage(
            params[0],
            params[1],
            params[2],
            image.$key,
            image.uuid,
            image.fileName
          );
        });
    } else {
      this.question.deleteTempImage(image.uuid, image.fileName).then((_) => {
        const index = this.newQuestionImages.findIndex((a) => {
          return a.uuid === image.uuid;
        });
        if (index !== -1) {
          this.newQuestionImages.splice(index, 1);
        }
      });
    }
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { McqQuestion } from '../../../../shared/question-types/mcq-question';

@Component({
  selector: 'ac-mcq-question-editor',
  templateUrl: './mcq-question-editor.component.html',
  styleUrls: ['./mcq-question-editor.component.scss']
})
export class McqQuestionEditorComponent implements OnInit {
  @Input() question: McqQuestion;
  @Output() update = new EventEmitter<McqQuestion>();
  questionForm: FormGroup;

  constructor() {}

  ngOnInit() {
    this.questionForm = new FormGroup({
      questionText: new FormControl(
        this.question.questionText,
        Validators.required
      ),
      choices: new FormArray([]),
      deleted: new FormControl(this.question.deleted),
      type: new FormControl('mcq'),
      keyText: new FormControl(this.question.keyText, Validators.required)
    });
    if (this.question.choices) {
      for (let i = 0; i <= this.question.choices.length - 1; i++) {
        (this.questionForm.get('choices') as FormArray).push(
          new FormGroup({
            choiceText: new FormControl(
              this.question.choices[i].choiceText,
              Validators.required
            ),
            correct: new FormControl(this.question.choices[i].correct)
          })
        );
      }
    }
  }

  get choices() {
    return this.questionForm.get('choices') as FormArray;
  }

  addChoice() {
    this.choices.push(
      new FormGroup({
        choiceText: new FormControl('', Validators.required),
        correct: new FormControl(false)
      })
    );
  }

  removeChoice(index: number) {
    this.choices.removeAt(index);
  }

  save() {
    if (this.questionForm.valid) {
      this.questionForm.disable();
      this.update.emit(this.questionForm.value);
    }
  }
}

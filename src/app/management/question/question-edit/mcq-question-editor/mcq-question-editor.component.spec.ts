import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { McqQuestionEditorComponent } from './mcq-question-editor.component';

describe('McqQuestionEditorComponent', () => {
  let component: McqQuestionEditorComponent;
  let fixture: ComponentFixture<McqQuestionEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ McqQuestionEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McqQuestionEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: proper tests should be created
});

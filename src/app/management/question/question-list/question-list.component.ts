import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { QuestionService } from '../../../core/question.service';
import { Chapter } from '../../../shared/chapter';
import { Course } from '../../../shared/course';
import { Question } from '../../../shared/question';
import { McqQuestion } from '../../../shared/question-types/mcq-question';

@Component({
  selector: 'ac-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  questions$: Observable<(Question & { $key: string })[]>;
  questionGists$: Observable<string[]>;
  courseKey$: Observable<string>;
  chapterKey$: Observable<string>;
  course$: Observable<Course>;
  chapter$: Observable<Chapter>;

  constructor(
    private route: ActivatedRoute,
    private question: QuestionService
  ) {}

  ngOnInit() {
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.chapterKey$ = this.route.data.pipe(map((data) => data.chapterKey));
    this.course$ = this.route.data.pipe(map((data) => data.course));
    this.chapter$ = this.route.data.pipe(map((data) => data.chapter));
    this.questions$ = combineLatest(this.courseKey$, this.chapterKey$).pipe(
      switchMap((param) => {
        return this.question.listQuestions(param[0], param[1]);
      }),
      map((data) => {
        return data.map((q) => {
          return {
            ...q.data(),
            $key: q.id
          };
        });
      })
    );
  }

  questionKey(q: Question) {
    return q.$key;
  }

  getQuestionGist(q: Question) {
    switch (q.type) {
      case 'mcq':
        return (q as McqQuestion).questionText;
      default:
        return '';
    }
  }
}

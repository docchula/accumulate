import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestionListComponent } from './question-list.component';
import { RouterModule, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../../core/question.service';
import { of } from 'rxjs';

class ActivatedRouteStub {
  data = of({
    course: {},
    courseKey: '',
    chapter: {},
    chapterKey: ''
  });
}

class QuestionServiceStub {
  listQuestions(courseKey: string, chapterKey: string) {
    return of([]);
  }
}

describe('QuestionListComponent', () => {
  let component: QuestionListComponent;
  let fixture: ComponentFixture<QuestionListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionListComponent ],
      imports: [ RouterModule ],
      providers: [
        {
          provide: ActivatedRoute,
          useClass: ActivatedRouteStub
        },
        {
          provide: QuestionService,
          useClass: QuestionServiceStub
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO: proper tests should be created
});

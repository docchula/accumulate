import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, shareReplay, switchMap } from 'rxjs/operators';
import { QuestionService } from '../../../core/question.service';
import { Chapter } from '../../../shared/chapter';
import { Course } from '../../../shared/course';

@Component({
  selector: 'ac-chapter-list',
  templateUrl: './chapter-list.component.html',
  styleUrls: ['./chapter-list.component.scss']
})
export class ChapterListComponent implements OnInit {
  chapters$: Observable<(Chapter & { $key: string })[]>;
  course$: Observable<Course>;
  courseKey$: Observable<string>;
  editingChapterKey: string = null;
  editingForm: FormGroup;
  newForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private question: QuestionService
  ) {}

  ngOnInit() {
    this.courseKey$ = this.route.data.pipe(map((data) => data.courseKey));
    this.course$ = this.route.data.pipe(map((data) => data.course));
    this.chapters$ = this.courseKey$.pipe(
      switchMap((courseKey) => {
        return this.question.listChapter(courseKey);
      }),
      map((data) => {
        return data.map((snap) => {
          return {
            ...snap.data(),
            $key: snap.id
          };
        });
      }),
      shareReplay(1)
    );
    this.editingForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
    this.newForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
  }

  updateChapter(chapterKey: string) {
    if (this.editingForm.valid) {
      this.courseKey$.pipe(first()).subscribe((courseKey) => {
        this.question
          .updateChapter(courseKey, chapterKey, this.editingForm.value)
          .then(() => {
            this.editingChapterKey = null;
          });
      });
    }
  }

  createChapter() {
    if (this.newForm.valid) {
      this.courseKey$.pipe(first()).subscribe((courseKey) => {
        this.question
          .createChapter(courseKey, this.newForm.value)
          .then(() => this.newForm.reset());
      });
    }
  }

  removeChapter(chapterKey: string) {
    this.courseKey$.pipe(first()).subscribe((courseKey) => {
      this.question.removeChapter(courseKey, chapterKey);
    });
  }

  editChapter(chapterKey: string, chapter: Chapter) {
    this.editingForm.setValue({
      name: chapter.name
    });
    this.editingChapterKey = chapterKey;
  }
}

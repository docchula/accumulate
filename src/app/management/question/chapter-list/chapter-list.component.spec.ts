import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, ActivatedRoute } from '@angular/router';

import { ChapterListComponent } from './chapter-list.component';
import { QuestionService } from '../../../core/question.service';
import { of } from 'rxjs';

class QuestionServiceStub {
  listChapter() {
    return of([]);
  }
}

class ActivatedRouteStub {
  data = of({
    code: '123',
    abbr: 'ASDF'
  });
}

describe('ChapterListComponent', () => {
  let component: ChapterListComponent;
  let fixture: ComponentFixture<ChapterListComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [ReactiveFormsModule, RouterModule],
        declarations: [ChapterListComponent],
        providers: [
          {
            provide: QuestionService,
            useClass: QuestionServiceStub
          },
          {
            provide: ActivatedRoute,
            useClass: ActivatedRouteStub
          }
        ]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // TODO: create proper test
});

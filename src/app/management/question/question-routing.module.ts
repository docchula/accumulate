import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../../role.guard';
import { ChapterKeyResolver } from '../../shared/chapter-key-resolver';
import { ChapterResolver } from '../../shared/chapter-resolver';
import { CourseKeyResolver } from '../../shared/course-key-resolver';
import { CourseResolver } from '../../shared/course-resolver';
import { QuestionKeyResolver } from '../../shared/question-key-resolver';
import { QuestionResolver } from '../../shared/question-resolver';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { CourseListComponent } from './course-list/course-list.component';
import { QuestionEditComponent } from './question-edit/question-edit.component';
import { QuestionListComponent } from './question-list/question-list.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RoleGuard],
    data: {
      allowedRoles: ['editor']
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: CourseListComponent
      },
      {
        path: ':courseKey',
        resolve: {
          course: CourseResolver,
          courseKey: CourseKeyResolver
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: ChapterListComponent
          },
          {
            path: ':chapterKey',
            resolve: {
              chapter: ChapterResolver,
              chapterKey: ChapterKeyResolver
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                component: QuestionListComponent
              },
              {
                path: ':questionKey/edit',
                component: QuestionEditComponent,
                resolve: {
                  question: QuestionResolver,
                  questionKey: QuestionKeyResolver
                },
                data: {
                  editMode: 'edit'
                }
              },
              {
                path: 'new',
                component: QuestionEditComponent,
                data: {
                  editMode: 'new'
                }
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    CourseResolver,
    ChapterResolver,
    CourseKeyResolver,
    ChapterKeyResolver,
    QuestionResolver,
    QuestionKeyResolver
  ]
})
export class QuestionRoutingModule {}

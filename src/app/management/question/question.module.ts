import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { MarkedModule } from '../../shared/marked/marked.module';
import { SharedModule } from '../../shared/shared.module';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { CourseListComponent } from './course-list/course-list.component';
import { McqQuestionEditorComponent } from './question-edit/mcq-question-editor/mcq-question-editor.component';
import { QuestionEditComponent } from './question-edit/question-edit.component';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionRoutingModule } from './question-routing.module';

@NgModule({
  imports: [
    CommonModule,
    QuestionRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    MarkedModule,
    ClipboardModule
  ],
  declarations: [
    CourseListComponent,
    ChapterListComponent,
    QuestionListComponent,
    QuestionEditComponent,
    McqQuestionEditorComponent
  ]
})
export class QuestionModule {}

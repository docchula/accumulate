import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { QuestionService } from '../../../core/question.service';
import { Course } from '../../../shared/course';

@Component({
  selector: 'ac-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {
  courses$: Observable<(Course & { $key: string })[]>;
  editingCourseKey: string = null;
  editingForm: FormGroup;
  newForm: FormGroup;
  reports$: Observable<any[]>;
  constructor(private question: QuestionService) {}

  ngOnInit() {
    this.courses$ = this.question.listCourses().pipe(
      map((data) => {
        return data.map((snap) => {
          return {
            ...snap.data(),
            $key: snap.id
          };
        });
      })
    );
    this.editingForm = new FormGroup({
      code: new FormControl('', Validators.required),
      nameTH: new FormControl('', Validators.required),
      nameEN: new FormControl('', Validators.required),
      abbr: new FormControl('', Validators.required)
    });
    this.newForm = new FormGroup({
      code: new FormControl('', Validators.required),
      nameTH: new FormControl('', Validators.required),
      nameEN: new FormControl('', Validators.required),
      abbr: new FormControl('', Validators.required)
    });
    this.reports$ = this.question.listReport();
  }

  removeCourse(courseKey: string) {
    this.question.removeCourse(courseKey);
  }

  editCourse(courseKey: string, course: Course) {
    this.editingForm.setValue({
      code: course.code,
      nameTH: course.nameTH,
      nameEN: course.nameEN,
      abbr: course.abbr
    });
    this.editingCourseKey = courseKey;
  }

  updateCourse(courseKey: string) {
    if (this.editingForm.valid) {
      this.question.updateCourse(courseKey, this.editingForm.value).then(() => {
        this.editingCourseKey = null;
      });
    }
  }

  createCourse() {
    if (this.newForm.valid) {
      this.question.createCourse(this.newForm.value).then(() => {
        this.newForm.reset();
      });
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../role.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RoleGuard],
    data: {
      allowedRoles: ['admin', 'editor']
    },
    children: [
      {
        path: '',
        pathMatch: 'exact',
        component: HomeComponent
      },
      {
        path: 'questions',
        canLoad: [RoleGuard],
        loadChildren: () =>
          import('./question/question.module').then((m) => m.QuestionModule),
        data: {
          allowedRoles: ['editor']
        }
      },
      {
        path: 'roles',
        canLoad: [RoleGuard],
        loadChildren: () =>
          import('./roles/roles.module').then((m) => m.RolesModule),
        data: {
          allowedRoles: ['admin']
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule {}

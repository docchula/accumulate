import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RouterLinkStubDirective } from './router-stubs';

@NgModule({
  imports: [CommonModule],
  declarations: [RouterLinkStubDirective],
  exports: [RouterLinkStubDirective]
})
export class TestingModule {}

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AuthService } from './core/auth.service';

@Injectable()
export class RoleGuard implements CanActivate, CanLoad {
  constructor(private auth: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.canLoadRoute(next);
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.canLoadRoute(route);
  }

  private canLoadRoute(route: Route | ActivatedRouteSnapshot) {
    return this.auth.roles.pipe(
      take(1),
      map((roles) => {
        const allowedRoles = route.data.allowedRoles as string[];
        let found = false;
        for (let i = 0; i <= roles.length - 1 && !found; i++) {
          if (allowedRoles.indexOf(roles[i]) !== -1) {
            found = true;
          }
        }
        return found;
      })
    );
  }
}

import {
  DocumentData,
  FirestoreDataConverter,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue
} from '@angular/fire/firestore';
import { UserResponse } from '../shared/user-response';

export const userResponseConverter: FirestoreDataConverter<UserResponse> = {
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions
  ) => {
    const data = snapshot.data(options);
    return {
      createdAt: data.createdAt,
      done: data.done,
      score: data.score,
      totalScore: data.totalScore,
      updatedAt: data.updatedAt
    };
  },
  toFirestore: (modelObject: WithFieldValue<UserResponse>) => {
    return { ...modelObject };
  }
};

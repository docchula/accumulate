import {
  DocumentData,
  FirestoreDataConverter,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue
} from '@angular/fire/firestore';
import { Chapter } from '../shared/chapter';

export const chapterConverter: FirestoreDataConverter<Chapter> = {
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions
  ) => {
    const data = snapshot.data(options);
    return {
      name: data.name,
      deleted: data.deleted
    };
  },
  toFirestore: (modelObject: WithFieldValue<Chapter>) => {
    return { ...modelObject };
  }
};

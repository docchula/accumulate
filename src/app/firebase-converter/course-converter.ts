import {
  DocumentData,
  FirestoreDataConverter,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue
} from '@angular/fire/firestore';
import { Course } from '../shared/course';

export const courseConverter: FirestoreDataConverter<Course> = {
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions
  ) => {
    const data = snapshot.data(options);
    return {
      abbr: data.abbr,
      code: data.code,
      deleted: data.deleted,
      nameEN: data.nameEN,
      nameTH: data.nameTH
    };
  },
  toFirestore: (modelObject: WithFieldValue<Course>) => {
    return { ...modelObject };
  }
};

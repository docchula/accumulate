import {
  DocumentData,
  FirestoreDataConverter,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue
} from '@angular/fire/firestore';
import { QuestionResponse } from '../shared/question-response';

export const questionResponseConverter: FirestoreDataConverter<QuestionResponse> =
  {
    fromFirestore: (
      snapshot: QueryDocumentSnapshot<DocumentData>,
      options?: SnapshotOptions
    ) => {
      const data = snapshot.data(options);
      return {
        answer: data.answer,
        correct: data.correct,
        graded: data.graded
      };
    },
    toFirestore: (modelObject: WithFieldValue<QuestionResponse>) => {
      return { ...modelObject };
    }
  };

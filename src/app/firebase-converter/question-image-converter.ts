import {
  DocumentData,
  FirestoreDataConverter,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue
} from '@angular/fire/firestore';
import { QuestionImage } from '../shared/question-image';

export const questionImageConverter: FirestoreDataConverter<QuestionImage> = {
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions
  ) => {
    const data = snapshot.data(options);
    return {
      fileName: data.fileName,
      imageUrl: data.imageUrl,
      uuid: data.uuid
    };
  },
  toFirestore: (modelObject: WithFieldValue<QuestionImage>) => {
    return { ...modelObject };
  }
};

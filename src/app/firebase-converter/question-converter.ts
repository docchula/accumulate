import {
  DocumentData,
  FirestoreDataConverter,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue
} from '@angular/fire/firestore';
import { Question } from '../shared/question';

export const questionConverter: FirestoreDataConverter<Question> = {
  fromFirestore: (
    snapshot: QueryDocumentSnapshot<DocumentData>,
    options?: SnapshotOptions
  ) => {
    const data = snapshot.data(options);
    return {
      $key: data.$key,
      choices: [...data.choices],
      deleted: data.deleted,
      keyText: data.keyText,
      questionText: data.questionText,
      type: data.type
    };
  },
  toFirestore: (modelObject: WithFieldValue<Question>) => {
    return { ...modelObject };
  }
};

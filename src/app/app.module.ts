import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import * as marked from 'marked';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { MARKED, MARKED_OPTIONS } from './shared/marked/di';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, SharedModule, CoreModule],
  providers: [
    {
      provide: MARKED_OPTIONS,
      useValue: {
        gfm: true,
        tables: true,
        breaks: true,
        sanitize: true
      }
    },
    {
      provide: MARKED,
      useValue: marked
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

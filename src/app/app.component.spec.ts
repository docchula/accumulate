import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject } from 'rxjs';

import { AppComponent } from './app.component';
import { AuthService } from './core/auth.service';
import { SharedModule } from './shared/shared.module';

class AuthServiceStub {
  user = new Subject();
}

describe('AppComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, SharedModule],
        declarations: [AppComponent],
        providers: [
          {
            provide: AuthService,
            useClass: AuthServiceStub
          }
        ]
      }).compileComponents();
    })
  );

  // TODO: proper tests should be created

  it(
    'should create the app',
    waitForAsync(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      fixture.detectChanges();
      expect(app).toBeTruthy();
    })
  );
});

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAYRrxsaN44DWBJYfxg_BNZPvkBsOSjqJk',
    authDomain: 'accumulate-e4740.firebaseapp.com',
    databaseURL: 'https://accumulate-e4740.firebaseio.com',
    projectId: 'accumulate-e4740',
    storageBucket: 'accumulate-e4740.appspot.com',
    messagingSenderId: '313654664559'
  }
};
